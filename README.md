
# Bakery Challenge
[![build status](https://gitlab.com/getmetorajesh/BakeryChallenge/badges/master/build.svg)](https://gitlab.com/getmetorajesh/BakeryChallenge/commits/master)

## HOW TO RUN
If `sbt` or `activator` is not installed. Install via brew
```sh
> brew install sbt
> sbt --version // check if the sbt version is >= 0.13.12

> git clone {REPO_URL}

// to Run
> sbt run
```

Or use docker
```sh 
docker build -t challenge .
docker run -ti -v $PWD:/app challenge

root@challenge:/app> sbt run
root@challenge:/app> sbt clean coverage test coverageReport

```

### Flow:
{ORDER(qty, product_code)} --> {CART[ORDER]} --> CHECKOUT --> {PROCESS_CART} --> {DISPLAYRESULTS, CLEAR_CART}

### Test:
run `sbt test`

### Test Coverage:
run `sbt clean coverage test coverageReport`

### Assumptions:
- Assuming unlimited supply of packs for all the products listed.
- Assuming the orders entered in command line.

### Sample Input/Output:
```
Enter Order(or CO(checkout) or C(CANCEL))> 10 VS5
Enter Order(or CO(checkout) or C(CANCEL))> CO
OUTPUT: 
10 VS5 $17.98
    2 x 5 $8.99


CART CLEARED:
 ********* NEW BILLING: ********** 
```

### Further Enhancements: 
There are few more enhancements that can made to this code base
- Refactor the println output to separate functions so that they can also be tested
- `Memoize` the Utils.splitPacks function to reuse already processed results
- Move the println statements into one function and use constants to display the values. So that this can be TESTED.

#### Task Breakdown:
- [X] 1. Get input from command line and parse it. 
- [X] 2. create models for data structure. add data in. display available product options.
- [X] 3. Create utils for splitting the order 
- [X] 4. Process all the individual order items
- [X] 5. Output the processed result

