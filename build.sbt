name := """BakeryChallenge"""

version := "1.0"

scalaVersion := "2.11.8"

//coverageEnabled := true

// Change this to another test framework if you prefer
libraryDependencies ++= Seq(
  "org.scalactic" %% "scalactic" % "3.0.0",
  "org.scalatest" %% "scalatest" % "3.0.0" % "test"
)

// Uncomment to use Akka
//libraryDependencies += "com.typesafe.akka" %% "akka-actor" % "2.3.11"

