package com.billcap
import scala.util.control.NonFatal

/**
  * Created by rajesh on 15/09/2016.
  * Utility functions
  */
object Utils {
  /**
    * Safely convert String to Int
    * @param s
    * @return Option[Int]
    */
  def toInt(s: String): Option[Int] = {
    try {
      Some(s.toInt)
    } catch {
      case e: Exception => None
    }
  }

  /**
    * Validate product code and return Some(code) if valid
    * @param givenCode
    * @param products
    * @return
    */
  def toValidProductCode(givenCode: String, products: List[Product]): Option[String] = {
    try {
      products.filter(_.code equals givenCode).head match {
        case _:Product => Some(givenCode)
        case _ => None
      }
    } catch {
      case e: Exception => None
    }
  }



  case class splitFor(items:Int, fromPackets: List[Int])

  /**
    * Given a splitFor items Int and List of available packs
    * it will return least split for the sum
    * @param s: splitFor(items:Int, fromPackets: List[Int])
    * @return Option[List[Int]]
    */
  def splitPacks(s: splitFor) : Option[List[Int]]= {
    var listOfPackets = List[Seq[Int]]()
    def changePackets(capacity: Int, availablePacks: List[Int], listOfPacks: Option[Seq[Int]]): Int = {
      if (capacity == 0) {
        listOfPackets = listOfPacks.get :: listOfPackets
        1
      }
      else if ((capacity < 0) | (availablePacks.isEmpty && capacity >= 1))
        0
      else {
        changePackets(capacity, availablePacks.tail, listOfPacks) +
          changePackets(capacity - availablePacks.head, availablePacks,
            Some(availablePacks.head +: listOfPacks.getOrElse(Seq())))
      }
    }

    try {
      changePackets(s.items, s.fromPackets.sortWith(_.compareTo(_) < 0), None)
    }catch {
      case e:Throwable => println("Unable to process order."+e)
    }

    listOfPackets.length > 0 match {
      case true => {
        // find the List with the least number of elements
        val least = listOfPackets.reduceLeft((first, second) =>{
          if(first.length < second.length) first else second
        })
        return Some(least.toList)
      }
      case _=> {
        return None
      }
    }
  }



}