package com.billcap

/**
  * Created by rajesh on 15/09/2016.
  */
case class Order(qty: Int, productCode: String)
case class Product(name: String, code: String, packs: List[ProductPack])
case class ProductPack(qty: Int, price: Double)