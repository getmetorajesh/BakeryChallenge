package com.billcap
import scala.collection.mutable.ListBuffer

case class AppException(message: String) extends Exception(message)
/**
  * Main class for BakeryChallenge
  */
object BakeryChallenge {

  // contains all the items order
  val cart: ListBuffer[Order] = ListBuffer()
  val products: List[Product] = List(ProductData.vegemite,
                                      ProductData.blueberry,
                                      ProductData.croissant)

  case class ItemSplit(count:Int, totalQty: Int, price: Double)
  case class ItemBill(order:Order, itemSplit:Option[List[ItemSplit]])


  def main(args: Array[String]): Unit = {
    //1.display avail options
    displayProducts()
    try {
      getInput()
    } catch {
      case AppException(ex) => println(ex)
    }
  }

  /**
    * Get input is a recursive function call asks
    * for user input until checkout | cancel
    */
  def getInput(): Unit = {
    val ln = readLine("Enter Order(or CO(checkout) or C(CANCEL))> ")
    processRawInput(ln)
    getInput()
  }

  /**
    * Decides what to do(Cancel, Checkout or Add to Cart) based on the input
    * @param ln: String
    */
  def processRawInput(ln: String): Unit = {
    ln match {
      case "CHECKOUT" | "CO" => {
        val inv: ListBuffer[ItemBill] = cart.map(single_order => {
          val single_item: Option[List[ItemSplit]] = processOrder(single_order)
          val item_split_bill: Option[List[ItemSplit]] = single_item
          new ItemBill(single_order, item_split_bill)
        })

        invoice(inv.toList)
        cart.clear()
        println("CART CLEARED:\n ********* NEW BILLING: ********** \n")
      }
      case "CANCEL" | "C" => {
        throw AppException("Q")
      }
      case inp: String => {
        parseOrder(inp) match {
          case Some(order) => cart.append(order)
          case None => {
           println("Invalid Order. Please try again")
          }
        }
        return
      }
    }
  }

  /**
    * Prints the invoice for the given order
    * @param itemised
    */
  def invoice(itemised: List[ItemBill]): Unit = {
    println("OUTPUT: ")
    itemised.foreach(bill_items => {
      val item_splits:Option[List[ItemSplit]] = bill_items.itemSplit
      item_splits match {
        case  Some(item_split) => {
          val total: Double = getTotalCostPer(item_split)

          println(s"""${bill_items.order.qty} ${bill_items.order.productCode} $$${total}""")
          item_split.map(split => {
            println(s"""    ${split.count} x ${split.totalQty} $$${split.price}""")
          })
          println("\n")
        }
        case None => {
          println("Unable to process this order.")
        }
      }
    })
  }

  /**
    * Get total cost for the given itemised bill
    * @param item_bill
    * @return
    */
  def getTotalCostPer(item_bill: List[ItemSplit]): Double = {
    val total: List[Double] = item_bill.map(split => {
      split.count *  split.price
    })
    return "%.2f".format((total.reduceLeft(_+_))).toDouble
  }


  /**
    * Parse string and convert to an Order object
    * @param input String
    * @return Option[Order]
    */
  def parseOrder(input: String): Option[Order] = {
    if (input.toString().length == 0) {
      return None
    }
    val ip = input.split(" ")
    if(ip.length != 2) return None

    val qty = Utils.toInt(ip(0))
    val code: Option[String] = Utils.toValidProductCode(ip(1), products)

    (qty.nonEmpty && code.nonEmpty) match {
      case true => return Some(new Order(qty.get, code.get))
      case _ => return None
    }
  }

  /**
    * Display all the products and available options
    */
  def displayProducts(): Unit = {
    println("Name Code Packs")
    products.map(product => {
      println(s"\n${product.name}")
      product.packs.map(pack => {
        println(s"${pack.qty} @ ${pack.price}")
      })
    })
  }

  /**
    * Given an Order detail it will return a results with split packs and price detailss
    * @param order
    */
  def processOrder(order: Order): Option[List[ItemSplit]] = {

    val product: Product = products.filter(_.code equals order.productCode).head
    val availablePacks = product.packs.map(p => p.qty)

    val split_for = new Utils.splitFor(order.qty, availablePacks)
    val splitPacks: Option[List[Int]] = Utils.splitPacks(split_for)

    // Store itemised bill for each items from the cart(i.e order)
    val itemised:ListBuffer[ItemSplit] = ListBuffer()

    splitPacks match {
      case None => None
      case Some(split) => {
        val distinctPacks = split.distinct
        // get how many packs
        distinctPacks.map { d => // how many 2 packs need to be packed
          val count = split.count(_ == d)
          val pack:ProductPack = product.packs.filter(p => p.qty == d).head
          itemised.append(new ItemSplit(count, pack.qty, pack.price))
          //println("itemised "+itemised)
          itemised.toList
        }
        return Some(itemised.toList)
      }
    }
    return None
  }
}
