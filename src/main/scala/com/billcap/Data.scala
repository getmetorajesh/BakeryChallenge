package com.billcap

import scala.collection.mutable.ListBuffer

/**
  * Created by rajesh on 15/09/2016.
  */
object ProductData {
  val vegemite = new Product("Vegemite Scroll", "VS5",
    List(new ProductPack(3, 6.99), new ProductPack(5, 8.99)));

  val blueberry = new Product("Blueberry Muffin", "MB11",
    List(new ProductPack(2, 9.95), new ProductPack(5, 16.95), new ProductPack(8, 24.95)));

  val croissant = new Product("Croissant", "CF",
    List(new ProductPack(3, 5.95), new ProductPack(5, 9.95), new ProductPack(9, 16.99)));

}
