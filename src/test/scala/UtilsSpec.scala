package com.billcap.test
import com.billcap.Utils.splitFor
import com.billcap._
import org.scalatest._

/**
  * Created by rajesh on 15/09/2016.
  */
class UtilsSpec extends FlatSpec with Matchers with OptionValues {
  val products: List[Product] = List(ProductData.vegemite,
    ProductData.blueberry,
    ProductData.croissant)

  "toValidProductCode" should "verify and return product code in a given product list" in {
    val prodCode = "VS5"
    val res = Utils.toValidProductCode(prodCode, products)
    assert(res equals Some(prodCode))
  }

  it should "return None for invalid product code" in {
    val prodCode = "UNKNOWN_PROD_CODE"
    val res = Utils.toValidProductCode(prodCode, products)
    assert(res equals None)
  }

  "toInt" should "return None for invalid product code" in {
    val res = Utils.toInt("twelve")
    assert(res equals None)
  }

  it should "return Some(val) for a valid Int String" in {
    val res = Utils.toInt("12")
    assert(res equals Some(12))
  }

  "SplitPacks" should "return List(5,5) for available packs List(3,5) and total:10" in {
    val given = new splitFor(10, List(3,5))
    val res = Utils.splitPacks(given)
    res.value shouldBe List(5,5)
  }

  it should "return None for available packs List(3,5) and total:7" in {
    val given = new splitFor(7, List(3,5))
    val res = Utils.splitPacks(given)
    res shouldBe None
  }

  it should "return List(8, 8, 8, 8, 8, 8, 8, 8, 7, 7) for available packs List(3, 5, 6, 7, 8) and total:78" in {
    val given = new splitFor(78, List(3, 5, 6, 7, 8))
    val res = Utils.splitPacks(given)
    res.value shouldBe List(8, 8, 8, 8, 8, 8, 8, 8, 7, 7)
  }

}
