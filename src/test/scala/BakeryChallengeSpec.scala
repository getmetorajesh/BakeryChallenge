package com.billcap.test
import com.billcap.BakeryChallenge.{ItemBill, ItemSplit}
import com.billcap._
import org.scalatest._

import scala.collection.mutable.ListBuffer

class BakeryChallengeSpec extends FlatSpec with Matchers {

  "processRawInput" should "exit if CANCEL | C is pressed" in {
    val in = "C"
    assertThrows[AppException] {
      BakeryChallenge.processRawInput(in)
    }
    val in2 = "CANCEL"
    assertThrows[AppException] {
      BakeryChallenge.processRawInput(in2)
    }
  }


  it should "should process result throw AppException" in {
    //reset cart
    BakeryChallenge.cart.clear()
    val valid = "12 VS5"
    BakeryChallenge.processRawInput(valid)
    info(s"${BakeryChallenge.cart}")

  }

  it should "add to order to cart if valid" in {
    //reset cart
    BakeryChallenge.cart.clear()
    val invalid = "12 VS"
    BakeryChallenge.processRawInput(invalid)
    assert(BakeryChallenge.cart.length === 0)

    val valid = "12 VS5"
    BakeryChallenge.processRawInput(valid)
    assert(BakeryChallenge.cart.length === 1)
  }


  it should "processOrder and clear cart if CO  is pressed" in {
    val in = "10 VS5"
    BakeryChallenge.processRawInput(in)

    assert(BakeryChallenge.cart.length > 0)

    // CO
    BakeryChallenge.processRawInput("CO")
    assert(BakeryChallenge.cart.length === 0)
  }

  it should "process multiple orders in cart and show output when CHECKOUT is entered" in {
    BakeryChallenge.processRawInput("10 VS5")
    BakeryChallenge.processRawInput("15 CF")
    assert(BakeryChallenge.cart.length === 2)
    BakeryChallenge.processRawInput("CHECKOUT")
  }

  "parseOrder" should "return None if unexpected input is provided" in {
    assert(BakeryChallenge.parseOrder("10BS") === None)
    assert(BakeryChallenge.parseOrder("10 BS") === None)
    assert(BakeryChallenge.parseOrder("1   VS5") === None)
    assert(BakeryChallenge.parseOrder("") === None)
  }

  "processOrder" should "" in {
    val in = "10 VS5"
    val res = BakeryChallenge.processRawInput(in)
    BakeryChallenge.processRawInput("CO")
  }

  "getTotalCostPer" should "return a double value of 25.85" in {
    val in = List(new ItemSplit(2, 5, 9.95), new ItemSplit(1, 3, 5.95))
    val total = BakeryChallenge.getTotalCostPer(in)
    assert(total === 25.85)
  }

  "invoice" should "print Unable to process this order for invalid input" in {
    val in = List(ItemBill(Order(10,"VS5"),None))
    BakeryChallenge.invoice(in)
    // prints "Unable to process this order"
  }
}
